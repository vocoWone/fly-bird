// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
  @property
  text: string = "flyBird";

  @property(cc.Node)
  bg0: cc.Node = null;

  @property(cc.Node)
  bg1: cc.Node = null;

  @property(cc.Node)
  pipe0: cc.Node = null;

  @property(cc.Node)
  pipe1: cc.Node = null;

  @property(cc.Node)
  pipe2: cc.Node = null;

  @property(cc.Node)
  birdNode: cc.Node = null;

  @property(cc.Sprite)
  bird0: cc.Sprite = null;

  @property(cc.Sprite)
  bird1: cc.Sprite = null;

  @property(cc.Sprite)
  bird2: cc.Sprite = null;

  @property(cc.Sprite)
  bird3: cc.Sprite = null;

  @property(cc.Label)
  labelScore: cc.Label = null;

  @property(cc.Node)
  startButton: cc.Node = null;

  @property(cc.Node)
  reStartButton: cc.Node = null;

  @property(cc.Node)
  gameOverBackground: cc.Node = null;

  birdWingInterval: number = 0;

  distance: number = 0;

  speed: number = 0;

  birdFrames: Array<cc.Node> = [];

  bgFrames: Array<cc.Node> = [];

  pipeAccount: Array<cc.Node> = [];

  isRun: boolean = false;

  // LIFE-CYCLE CALLBACKS:

  onLoad() {
    this.bgFrames = [this.bg0, this.bg1];
    this.birdFrames = [
      this.bird0.node,
      this.bird1.node,
      this.bird2.node,
      this.bird3.node
    ];
    this.pipeAccount = [this.pipe0, this.pipe1, this.pipe2];

    // 管子初始化偏移
    this.pipe0.y = this.pipe0.y + this.getRandomInt(-80, 80);
    this.pipe1.y = this.pipe1.y + this.getRandomInt(-80, 80);
    this.pipe2.y = this.pipe2.y + this.getRandomInt(-80, 80);
  }

  start() {}

  update(dt: number) {
    // 小鸟翅膀动作
    this.birdWingInterval = this.birdWingInterval + dt;
    if (this.birdWingInterval > 0.2) {
      const currentActiveBirdIndex: number =
        this.birdFrames.findIndex(_ => _.active) || 0;
      const nextActiveBirdIndex: number =
        (currentActiveBirdIndex + 1) % this.birdFrames.length;
      this.birdFrames.forEach((_, index) => {
        if (currentActiveBirdIndex === index) {
          _.active = false;
        }
        if (nextActiveBirdIndex === index) {
          _.active = true;
        }
      });
      this.birdWingInterval = 0;
    }

    if (this.isRun) {
      // 小鸟位移
      this.bg0.x = this.bg0.x - 1;
      this.bg1.x = this.bg1.x - 1;
      this.bg0.x <= -this.bg0.width && (this.bg0.x = this.bg0.width);
      this.bg1.x <= -this.bg1.width && (this.bg1.x = this.bg1.width);

      // 重力加速度（上升与下降）
      const startSpeed = this.speed;
      const nextSpeed = startSpeed + 9.8 * dt;
      const distance =
        ((Math.pow(nextSpeed, 2) - Math.pow(startSpeed, 2)) / 2) * 9.8;
      this.birdNode.y = this.birdNode.y - distance;
      this.speed = nextSpeed;

      // 小鸟旋转
      const nextRotation = this.speed * 10;
      this.birdNode.angle = nextRotation > 90 ? 90 : -nextRotation;

      // 管子移动
      this.pipeAccount.forEach(_ => {
        _.x = _.x - 1;
        if (_.x <= -170) {
          // 积分
          this.labelScore.string = (
            Number(this.labelScore.string) + 1
          ).toString();
          _.x = 262;
          _.y = this.getRandomInt(-80, 80);
        }
      });

      // 小鸟是否超出边界计算
      if (
        this.birdNode.y + this.birdNode.height / 2 >= this.bg0.height / 2 ||
        this.birdNode.y - this.birdNode.height / 2 <= -this.bg0.height / 2
      ) {
        this.isRun = false;
        this.reStartButton.active = true;
        this.gameOverBackground.active = true;
      }

      // 检测碰撞
      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe0.x - this.pipe0.width / 2,
          right: this.pipe0.x + this.pipe0.width / 2,
          top: this.bg0.height / 2,
          bottom: this.pipe0.y + 50
        }
      );
      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe0.x - this.pipe0.width / 2,
          right: this.pipe0.x + this.pipe0.width / 2,
          top: this.pipe0.y - 50,
          bottom: -this.bg0.height / 2
        }
      );

      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe1.x - this.pipe1.width / 2,
          right: this.pipe1.x + this.pipe1.width / 2,
          top: this.bg0.height / 2,
          bottom: this.pipe1.y + 50
        }
      );
      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe1.x - this.pipe1.width / 2,
          right: this.pipe1.x + this.pipe1.width / 2,
          top: this.pipe1.y - 50,
          bottom: -this.bg0.height / 2
        }
      );

      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe2.x - this.pipe2.width / 2,
          right: this.pipe2.x + this.pipe2.width / 2,
          top: this.bg0.height / 2,
          bottom: this.pipe2.y + 50
        }
      );
      this.checkCollision(
        {
          left: this.birdNode.x - this.birdNode.width / 2,
          right: this.birdNode.x + this.birdNode.width / 2,
          top: this.birdNode.y + this.birdNode.height / 2,
          bottom: this.birdNode.y - this.birdNode.height / 2
        },
        {
          left: this.pipe2.x - this.pipe2.width / 2,
          right: this.pipe2.x + this.pipe2.width / 2,
          top: this.pipe2.y - 50,
          bottom: -this.bg0.height / 2
        }
      );
    }
  }

  onTouch() {
    this.speed = -3;
  }

  onStart() {
    this.isRun = true;
    this.startButton.active = false;
  }

  onReStart() {
    this.isRun = true;
    this.reStartButton.active = false;
    this.gameOverBackground.active = false;
    this.labelScore.string = "0";
    this.birdNode.x = 0;
    this.birdNode.y = 0;
    this.birdNode.angle = 0;
    this.speed = 0;

    this.pipe0.x = 170;
    this.pipe1.x = 314;
    this.pipe2.x = 456;
    this.pipe0.y = this.pipe1.y = this.pipe2.y = 0;
    this.pipe0.y = this.pipe0.y + this.getRandomInt(-80, 80);
    this.pipe1.y = this.pipe1.y + this.getRandomInt(-80, 80);
    this.pipe2.y = this.pipe2.y + this.getRandomInt(-80, 80);
  }

  checkCollision(
    bird: { left: number; right: number; top: number; bottom: number },
    pipe: { left: number; right: number; top: number; bottom: number }
  ) {
    // 左<右
    // 右>左
    // 上>下
    // 下<上
    const birdX0 = bird.left; // 左
    const birdX1 = bird.right; // 右
    const birdY0 = bird.top; // 上
    const birdY1 = bird.bottom; // 下

    const pipeX0 = pipe.left; // 左
    const pipeX1 = pipe.right; // 右
    const pipeY0 = pipe.top; // 上
    const pipeY1 = pipe.bottom; // 下

    if (
      birdX0 > pipeX1 ||
      birdX1 < pipeX0 ||
      birdY0 < pipeY1 ||
      birdY1 > pipeY0
    ) {
      return;
    } else {
      this.isRun = false;
      this.reStartButton.active = true;
      this.gameOverBackground.active = true;
    }
  }

  getRandomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
